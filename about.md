---
layout: default
title: About Rogue Rift
---

<div class="post">
              <h1 class="pageTitle">About Rogue Rift</h1><img alt="" src="/newdungeon.gif" width="956" height="547" />
              <p class="intro"><span style="font-size: 1.8rem; letter-spacing: 0.01rem;">Rogue Rift is a dungeon crawler created in the Unity Engine. The main aspect are leveling up your character by defeating enemies clearing dungeons and upgrading weapons.</span></p>
              <h2>Features</h2>
              <ul>
                  <li>Skill Tree</li>
                  <li>5 Different Regions with different mobs and bosses in each</li>
                  <li>&nbsp;</li>
              </ul>
          </div>